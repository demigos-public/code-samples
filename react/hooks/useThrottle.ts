import { useRef, useCallback, MutableRefObject } from 'react';

const useThrottle = (callback, delay) => {

    const isThrottled = useRef<MutableRefObject<boolean>>(null);

    const throttledCallback = useCallback((...args) => {
        if (isThrottled.current) {
            return
        }
        callback(...args);
        isThrottled.current = true
        setTimeout(() => isThrottled.current = false, delay)
    },[callback, delay])

    return throttledCallback
}

export default useThrottle;