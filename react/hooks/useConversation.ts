import { useMutation } from "@apollo/client";

import { ConversationAPI } from 'api';

const useConversation = () => {
    const [getTwilioToken, { data }] = useMutation(ConversationAPI.getTwilioToken())

    const getToken = (room) => {
        return getTwilioToken({
            variables: {
                input: { appointmentId: room }
            }
        })
            .then(response => response.data.getTwilioToken)
    }

    return { getToken, token: data?.getTwilioToken, room: data?.getTwilioToken.room };
}

export default useConversation;
