import { useEffect, useState } from "react";
import { Client } from '@twilio/conversations';
import {useSnackbar} from "notistack";

import { useConversation } from "hooks/conversation";

const useChat = (appointmentId, doctorId, patientId) => {

    const { getToken } = useConversation();
    const { enqueueSnackbar } = useSnackbar();

    const [ isLoading, setLoading ] = useState(true);
    const [ token, setToken ] = useState(null);
    const [ room, setRoom ] = useState(null);
    const [ conversation, setConversation ] = useState(null);

    const [ messages, setMessages ] = useState([]);

    useEffect(() => {
        if (appointmentId && doctorId) {
            getToken(appointmentId)
                .then(data => {
                    setToken(data.token);
                    setRoom(data.room)
                })
        }
    }, [ appointmentId, doctorId ])

    useEffect(() => {

        const  onConversationFound = (conversation) => {
            setConversation(conversation)
        }

        const  onConversationNotFound = () => {
            enqueueSnackbar('Something went wrong');
            setLoading(false)
        }

        if (token && room) {
            const client = new Client(token);
            client.on('stateChanged', (state) => {
                if (state === 'initialized') {
                    client.getConversationByUniqueName(room)
                        .then(onConversationFound)
                        .catch(onConversationNotFound)
                }
            });
        }
    }, [ token, room, appointmentId ])

    useEffect(() => {
        if (conversation) {
            conversation.getMessages()
                .then(({ items }) => {
                    setMessages(items);
                    setLoading(false)
                })

        } else {
            setMessages([]);
        }
    }, [ conversation ]);

    return {
        messages,
        isLoading,
        token
    }
}

export default useChat;
