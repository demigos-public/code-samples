import { makeStyles } from '@mui/styles'

const useLoginFormStyles = makeStyles({
    fieldsBlock: {
        gap: '16px',
        marginBottom: '40px',
    },
})

export default useLoginFormStyles
