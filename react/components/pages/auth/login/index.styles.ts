import { makeStyles } from '@mui/styles'

export default makeStyles({
    fieldsBlock: {
        gap: '16px',
        marginBottom: '40px',
    },
})