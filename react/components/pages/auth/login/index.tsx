import useLoginFormStyles from './index.styles'

import React from 'react'

import { useAuth } from 'hooks/useAuth'
import { LoginForm, LoginFormValues } from '../../../forms'

const LoginPage = (): JSX.Element => {
    const { signIn } = useAuth();

    const classes = useLoginFormStyles()

    const handleSubmit = ({ email, password }: LoginFormValues) => {
        signIn
    }

    return (
        <div className={classes.page}>
            <LoginForm onSubmit={} />
        </div>
    )
}

export default LoginPage
