import React from 'react';

import { ChatContainer } from 'components/conversation';

const ChatPage = () => <ChatContainer />

export default ChatPage;