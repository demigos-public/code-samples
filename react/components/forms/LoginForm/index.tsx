import styles from './index.styles'

import React from 'react'

import { useFormik } from 'formik'
import { Button, Stack } from '@mui/material'

import { PasswordField, TextField } from 'components/inputs'
import { LoginFormScheme } from './index.schema'
import { IAuthResponse } from 'hooks/useAuth'
import Loader from 'components/common/Loader'

interface LoginFormProps {
    isLoading: boolean
    onSubmit: (response: IAuthResponse) => void
}

const LoginForm = ({ onSubmit, isLoading }: LoginFormProps): JSX.Element => {

    const formik = useFormik({
        initialValues: {
            email: '',
            password: ''
        },
        validationSchema: LoginFormScheme,
        onSubmit,
    })

    const classes = styles()

    return (
        <div>
            {isLoading && <Loader />}
            <form onSubmit={formik.handleSubmit}>
                <Stack className={classes.fields}>
                    <TextField
                        autoFocus
                        id="email-input"
                        fullWidth
                        name="email"
                        label="Email Address"
                        placeholder="Enter your email"
                        value={formik.values.email}
                        onChange={formik.handleChange}
                        error={
                            formik.touched.email && Boolean(formik.errors.email)
                        }
                        helperText={formik.touched.email && formik.errors.email}
                    />
                    <PasswordField
                        autoFocus
                        id="password-input"
                        name="password"
                        label="Password"
                        placeholder="Enter your password"
                        value={formik.values.password}
                        onChange={formik.handleChange}
                        error={
                            formik.touched.password &&
                            Boolean(formik.errors.password)
                        }
                        helperText={
                            formik.touched.password && formik.errors.password
                        }
                        fullWidth
                    />
                </Stack>
                <Button
                    type="submit"
                    color="secondary"
                    disabled={isLoading}
                    fullWidth
                >
                    Login
                </Button>
            </form>
        </div>
    )
}

export default LoginForm
