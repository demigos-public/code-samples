import { object, string } from 'yup'

import { emailValidator } from '../index.validators'

export const LoginFormScheme = object({
    email: string()
        .test(emailValidator)
        .required('Email must be valid'),

    password: string()
        .required('Password is required'),
})
