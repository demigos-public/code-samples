import { makeStyles } from '@mui/styles'

export default makeStyles({
    fields: {
        gap: '16px',
        marginBottom: '40px',
    },
})