import { makeStyles } from '@mui/styles'

export default makeStyles({
    loader: {
        width: '100%',
        height: '100%',
    },
})