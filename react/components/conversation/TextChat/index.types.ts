export interface TextChatProps {
    readonly: boolean,
    token: string,
    channelName: string,
    userName: string,
    onConnected: (participant: User<PatientType> | User<DoctorType>) => void,
    doctor: User<DoctorType>,
    patient: User<PatientType>,
    isInProgress: boolean;
}