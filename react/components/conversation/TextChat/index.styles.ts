import {makeStyles} from '@mui/styles'

export default makeStyles({
    chat: {
        display: 'flex',
        flexDirection: 'column',
        flexGrow: 1,
        overflowY: 'hidden',
    },
    content: {
        overflowT: 'auto',
        display: 'flex',
        flexDirection: 'column',
        flexGrow: 1,
        marginBottom: '10px',
        padding: '0 5px',
    },
    footer: {
        width: '100%',
        backgroundColor: '#f05',
        display: 'flex',
        justifyContent: 'stretch',
    },
    icon: {
        width: '30px',
        height: '30px',
        color: '#000',
        padding: 0,
        cursor: 'pointer',
        transition: '.2s ease',
        display: 'flex',
        alignSelf: 'flex-start',
        margin: '5px',
    }
})