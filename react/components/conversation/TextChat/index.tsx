import styles from  './index.styles.ts';

import React, { useEffect, useRef, useState, memo } from 'react';
import { useIntl } from "react-intl";
import { useSnackbar } from "notistack";
import { Send } from '@material-ui/icons';
import { Client } from '@twilio/conversations';

import { TextField } from 'components/forms';
import { Message } from 'components/conversation';
import { useThrottle } from "hooks";

import { TextChatProps } from 'index.types';

const TextChat = memo ( ({
                             readonly = false,
                             token,
                             channelName,
                             userName,
                             onConnected,
                             doctor,
                             patient,
                             isInProgress
}: TextChatProps) => {

    const classes = styles()

    const intl = useIntl()
    const { enqueueSnackbar } = useSnackbar();

    const scrollableContainer = useRef<HTMLDivElement | null>(null);
    const [ conversation, setConversation ] = useState<Conversation | null>(null);
    const [ messages, setMessages ] = useState<Message[]>([]);
    const [ text, setText ] = useState<string>('');

    useEffect(() => {

        const onConversationFound = (conversation: Conversation) => {
            setConversation(conversation)
            conversation.on("messageAdded", handleNewMessage);
            onConnected();
        }

        if (token && channelName) {
            const client = new Client(token);
            client.on('stateChanged', (state: ClientState) => {
                if (state === 'initialized') {
                    client.getConversationByUniqueName(channelName)
                        .then(onConversationFound)
                        .catch(() => enqueueSnackbar('Something went wrong'))
                        .finally(() => onConnected())
                }
            });
        }
    }, [ token, channelName ]);

    useEffect(() => {
        if (conversation) {
            conversation.getMessages()
                .then((messages: Message[]) => {
                    setMessages(messages.items);
                    if (scrollableContainer.current) {
                        scrollChatToBottom(scrollableContainer.current);
                    }
                })

        } else {
            setMessages([]);
        }
    }, [ conversation, scrollableContainer ]);

    const isScrolledToBottom = (element: HTMLDivElement) => {
        return element.scrollHeight - element.clientHeight <= element.scrollTop + 1;
    }

    const isLocalMessage = (author: User<DoctorType> | User<PatientType>) => author === doctor.doctorId;

    const scrollChatToBottom = (element: HTMLDivElement) => {
        element.scrollTop = element.scrollHeight - element.clientHeight;
    }

    const handleNewMessage = (message: Message) => {

        let isOnBottom: boolean;

        if (scrollableContainer.current) {
            isOnBottom = isScrolledToBottom(scrollableContainer.current);
        }

        setMessages((currentMessages: Message[]) => [ ...currentMessages, message ]);

        if (isOnBottom && scrollableContainer.current) {
            scrollChatToBottom(scrollableContainer.current);
        }

    }

    const handleSendMessage = () => {
        const normalizedText = text.trim();
        if (normalizedText) {
            conversation.sendMessage(normalizedText, { userId: userName })
                .then(() => setText(""));
        }
    }

    const renderMessages = (messages: Message[]) => {
        return messages.map((message: Message) => {
            return <Message
                data={ message }
                local={ isLocalMessage(message.author) }
                key={ message.state.sid }
                avatar={ isLocalMessage(message.author) ? doctor?.avatar : patient?.avatar }
            />
        })
    }

    const throttledHandleSendMessage = useThrottle(handleSendMessage, 500)

    const handleEnterPressed = (e) => {
        if (e.keyCode === 13 && !e.shiftKey) {
            e.preventDefault();
            throttledHandleSendMessage();
        }
    }

    const handleChange = (e: InputEvent) => setText(e.target.value);

    const renderFooter = () => {
        return (
            <div className={classes.footer}>
                <TextField
                    disabled={!isInProgress}
                    multiline
                    maxRows={ 3 }
                    value={ text }
                    onChange={ handleChange }
                    onKeyDown={ handleEnterPressed }
                    placeholder={ intl.formatMessage({ id: 'words.common.type-message' }) }
                />
                <Send className={classes.icon}
                      onClick={ throttledHandleSendMessage }/>
            </div>
        )
    }

    const renderChat = (messages: Message[]) => {
        return (
            <>
                <div className={classes.content} ref={ scrollableContainer }>
                    { !!messages.length && renderMessages(messages) }
                </div>
                { !readonly && renderFooter() }
            </>
        )
    }


    return (
        <div className={classes.chat}>
            { conversation && renderChat(messages) }
        </div>
    )
},)

export default TextChat;
