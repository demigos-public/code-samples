export interface ChatContainerProps {
    identity: string,
    token: string,
    roomName: string,
    onDecline: () => void,
    isOnCall: boolean,
    doctor: User<DoctorType>,
    patient: User<PatientType>,
    endTime: Date,
    isInProgress: boolean;
}