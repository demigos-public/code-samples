import styles from  './index.styles.ts';

import React, { useEffect, useState, memo } from "react";

import { VideoChat, TextChat } from 'components/conversation';
import { BoxLoader } from 'components/layout';
import { ChatContainerProps } from "./index.types";

const ChatContainer = memo(({ identity, token, roomName, onDecline, isOnCall, doctor, patient, endTime, isInProgress }: ChatContainerProps) => {

    const classes = styles()

    const [ isLoading, setLoading ] = useState<boolean>(true);
    const [isTextChatConnected, setTextChatConnected] = useState<boolean>(false);

    useEffect(() => {
        if (isTextChatConnected) {
            setLoading(false);
        }
    }, [isTextChatConnected])

    const handleTextChatConnected = () => {
        setTextChatConnected(true);
    }

    const renderLoader = () => {
        return (
            <div className={classes.loader}>
                <BoxLoader />
                <div className={classes.loaderText}>Connecting</div>
            </div>
        )
    }
    return (
        <div className={classes.container}>
            <div className={!isLoading ? classes.connectedChats : classes.chats}>
                {isOnCall && (
                    <div className={classes.videoLayout}>
                        <VideoChat
                            token={token}
                            roomName={roomName}
                            onDecline={onDecline}
                            endTime={endTime}
                        />
                    </div>
                )}
                <TextChat
                    userName={identity}
                    token={token}
                    channelName={roomName}
                    onConnected={handleTextChatConnected}
                    doctor = { doctor }
                    patient = { patient }
                    isInProgress={ isInProgress }
                />
            </div>
            {isLoading && renderLoader() }
        </div>
    )
}, (prevProps, nextProps) => prevProps.isOnCall === nextProps.isOnCall && prevProps.isInProgress === nextProps.isInProgress);

export default ChatContainer;
