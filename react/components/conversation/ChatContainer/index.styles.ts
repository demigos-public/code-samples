import {makeStyles} from '@mui/styles'

export default makeStyles({
    container: {
        display: 'flex',
        flexGrow: 1,
        flexDirection: 'column',
        position: 'relative',
        width: '100%',
        justifyContent: 'stretch',
        alignItems: 'stretch',
        overflowY: 'hidden',
    },
    loader: {
        position: 'absolute',
        display: 'flex',
        flexDirection: 'column',
        marginTop: '40%',
        transform: 'translateY(50%)',
        width: '100%',
        alignItems: 'center'
    },
    loaderText: {
        marginTop: '75px',
        color: '#454545',
    },
    videoChat: {
        marginBottom: 0
    },
    videoLayout: {
        backgroundColor: '#ccc',
    },
    chats: {
        display: 'none',
        visibility: 'hidden',
    },
    connectedChats: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'stretch',
        flexGrow: 1,
        overflowY: 'hidden',
    }
})