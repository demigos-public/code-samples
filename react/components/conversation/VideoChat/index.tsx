import React, { useEffect, useState } from "react";
import Video from 'twilio-video';

import Room from './components/Room';

const VideoChat = ({ token, roomName, onConnected, onDisconnected, onDecline, endTime }) => {

    const [ room, setRoom ] = useState(null);
    const [ remoteMuted, setRemoteMuted ] = useState(false);
    const [ remotedCameraOn, setRemotedCameraOn ] = useState(true);

    useEffect(() => {
        if (token && roomName) {
            Video.connect(token, { name: roomName })
                .then(room => {
                    setRoom(room);
                    window.addEventListener('beforeunload', () => {
                        room.disconnect();
                    })
                    onConnected(room);
                })
        }
        return () => {
            disconnect();
        };
    }, [token, roomName]);

    const disconnect = () => {
        setRoom(currentRoom => {
            if (currentRoom && currentRoom.localParticipant.state === 'connected') {
                currentRoom.localParticipant.tracks.forEach(function(trackPublication) {
                    trackPublication.track.stop();
                });
                currentRoom.disconnect();
                onDisconnected();
                return null;
            } else {
                return currentRoom;
            }
        });
    }

    const handleDecline = () => {
        disconnect();
        onDecline();
    }

    return (
        <div className={'video-chat'}>
            {room ? <Room room={room} remoteMuted={remoteMuted} remotedCameraOn={remotedCameraOn} onDecline={handleDecline} endTime={endTime} /> : null}
        </div>
    )
}

export default VideoChat;
