export interface ParticipantProps {
    participant: User<DoctorType> | User<PatientType>,
    width: number,
    height: number,
    muted: boolean,
    cameraOn: boolean,
    noCameraImage: Image | null,
}