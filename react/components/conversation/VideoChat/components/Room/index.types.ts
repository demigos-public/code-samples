export interface RoomProps {
    room: string,
    onDecline: () => void,
    remoteMuted: boolean,
    remotedCameraOn: boolean,
    endTime: Date
}

export interface Countdown {
    minutes: string;
    seconds: string;
}