import styles from  './index.styles.ts';

import React, { useEffect, useState } from "react";
import { Mic, MicOff, Videocam, VideocamOff } from '@material-ui/icons';

import { useUser } from 'hooks/user';
import { getSecondsToTime } from "utils/date";
import Participant from "../Participant";
import RemoteParticipant from "../RemoteParticipant";

import { RoomProps, Countdown } from "./index.types";

import declineCall from 'assets/images/decline-call.svg';

const Room = ({ room, onDecline, remoteMuted, remotedCameraOn, endTime }: RoomProps) => {

    const classes = styles()

    const { user } = useUser();
    const [ countDown, setCountDown ] = useState<Countdown>({minutes: '--', seconds: '--'});

    useEffect(()=>{
        if (endTime) {
            let myInterval = setInterval(() => {
                const timeLeft = getSecondsToTime(endTime);
                if (timeLeft < 0) {
                    setCountDown({minutes: 0, seconds: '00'})
                    clearInterval(myInterval);
                } else if (timeLeft === 0) {
                    setCountDown({minutes: '--', seconds: '--'})
                } else {
                    const minutes = Math.trunc(timeLeft / 60);
                    const seconds = String(timeLeft % 60).padStart(2, '0');
                    setCountDown({ minutes, seconds })
                }
            }, 1000);
            return ()=> {
                clearInterval(myInterval);
            };
        }
    }, [endTime]);

    const [ patient, setPatient ] = useState<User<PatientType>>(null);
    const [ muted, setMuted ] = useState<boolean>(false);
    const [ cameraOn, setCameraOn ] = useState<boolean>(true);

    useEffect(() => {
        room.on('participantConnected', onPatientConnected);
        room.on('participantDisconnected', onPatientDisconnected);
        room.participants.forEach(onPatientConnected);
    }, [room])

    const onPatientConnected = (participant: User<PatientType>) => {
        setPatient(participant);
    }

    const onPatientDisconnected = () => {
        setPatient(null);
    }

    const handleToggleMute = () => {
        setMuted(prevState => !prevState);
    }

    const handleToggleCamera = () => {
        setCameraOn(prevState => !prevState);
    }
    return (
        <div className={classes.room}>
            {endTime && <div className={classes.countdown}>
                <span>{countDown.minutes}</span>
                <span>:</span>
                <span>{countDown.seconds}</span>
            </div> }
            <div className={classes.me}>
                {room && <Participant
                    participant={room.localParticipant}
                    key={room.localParticipant.sid}
                    width={120}
                    height={90}
                    muted={muted}
                    cameraOn={cameraOn}
                    noCameraImage={user.avatar}
                />}
            </div>
            <div className={classes.patient}>
                {patient && (
                    <RemoteParticipant
                        muted={remoteMuted}
                        cameraOn={remotedCameraOn}
                        participant={patient}
                        key={patient.sid}
                        width={400}
                        height={300}
                    />
                )}
            </div>
            <div className={classes.actions}>
                <div className={classes.action} onClick={handleToggleMute}>
                    {muted ? <MicOff /> : <Mic />}
                </div>
                <div className={classes.action} onClick={onDecline}>
                    <img src={declineCall} alt="decline call"/>
                </div>
                <div className={classes.action} onClick={handleToggleCamera}>
                    {cameraOn ? <Videocam /> : <VideocamOff />}
                </div>
            </div>
        </div>
    )
};

export default Room;