import styles from  '../Room/index.styles';

import React, { useState, useEffect, useRef } from "react";

import { ParticipantProps } from '../index.types';

const RemoteParticipant = ({ participant, width, height, muted = false, cameraOn = true, noCameraImage = null }: ParticipantProps) => {

    const classes = styles();

    const [videoTracks, setVideoTracks] = useState<VideoTrack[]>([]);
    const [audioTracks, setAudioTracks] = useState<AudioTrack[]>([]);

    const [videoIsEnabled, setVideoIsEnabled] = useState(true);

    const videoRef = useRef<HTMLVideoElement>();
    const audioRef = useRef<HTMLAudioElement>();

    const trackpubsToTracks = (trackMap: Map) =>
        Array.from(trackMap.values())
            .map((publication: Publication) => publication.track)
            .filter((track: Track) => track !== null);

    useEffect(() => {
        setVideoTracks(trackpubsToTracks(participant.videoTracks));
        setAudioTracks(trackpubsToTracks(participant.audioTracks));

        const trackSubscribed = (track: Track) => {
            if (track.kind === "video") {
                setVideoTracks((videoTracks) => [...videoTracks, track]);
            } else if (track.kind === "audio") {
                setAudioTracks((audioTracks) => [...audioTracks, track]);
            }
        };

        const trackUnsubscribed = (track: Track) => {

            if (track.kind === "video") {
                setVideoTracks((videoTracks) => videoTracks.filter((v: Track) => v !== track));
            } else if (track.kind === "audio") {
                setAudioTracks((audioTracks) => audioTracks.filter((a: Track) => a !== track));
            }
        };

        participant.on("trackSubscribed", trackSubscribed);
        participant.on("trackUnsubscribed", trackUnsubscribed);

        return () => {
            setVideoTracks([]);
            setAudioTracks([]);
            participant.removeAllListeners();
        };
    }, [participant]);

    useEffect(() => {
        const videoTrack = videoTracks[0];
        if (videoTrack) {

            videoTrack.attach(videoRef.current);
            videoTrack.on('enabled', () => {
                setVideoIsEnabled(true);
                videoTrack.attach(videoRef.current);
            });
            videoTrack.on('disabled', () => {
                setVideoIsEnabled(false);
                videoTrack.detach();
            })
            return () => {
                videoTrack.removeAllListeners();
                videoTrack.detach();
            };
        }
    }, [videoTracks]);

    useEffect(() => {
        const audioTrack = audioTracks[0];
        if (audioTrack) {
            audioTrack.attach(audioRef.current);
            return () => {
                audioTrack.detach();
            };
        }
    }, [audioTracks]);

    return (
        <div className={classes.participant}>
            {videoIsEnabled && <video ref={videoRef} autoPlay={true} width={width} height={height} />}
            <audio ref={audioRef} autoPlay={true} muted={false} />
        </div>
    );
};

export default RemoteParticipant;