import styles from  './index.styles.ts';

import React, { useEffect, useState } from "react";
import { AttachmentOutlined } from '@material-ui/icons';

import { formatISOtoHumanReadable, getDaysFromNow } from "utils/date";
import { MessageProps } from 'index.types';

import noAvatar from 'assets/images/no-avatar.svg';

const Message = ({ data, local, avatar, renderLocalAuthor = false }: MessageProps) => {

    const classes = styles()

    const [attachmentURL, setAttachmentURL] = useState<string>('');

    useEffect(() => {
        if (data.media) {
            data.media.getContentTemporaryUrl()
                .then((url: string) => {
                    setAttachmentURL(url);
                })
        }
    }, [])

    const isAttachment = (message: Message) => {
        return message.type === 'media';
    }

    const renderAuthor = () => {
        if (local && !renderLocalAuthor) return null;
        return (
            <div className={classes.author}>
                <img className={classes.avatar} src={avatar || noAvatar} alt="avatar"/>
            </div>
        )
    }

    const renderTextMessage = (message: Message) => {
        return <span className={classes.text}>{message.body}</span>
    }

    const renderAttachment = (media: MediaMessage) => {
        return (
            <a href={attachmentURL} target={'_blank'} download={'attachment'} className={classes.attachment}>
                <AttachmentOutlined className={classes.icon} />
                <span className={classes.filename}>{media?.filename || 'untitled'}</span>
            </a>
        )
    }

    return (
        <div className={local ? classes.localMessage : classes.remoteMessage}>
            {renderAuthor()}
            <div className={classes.data}>
                <div className={classes.content}>{isAttachment(data) ? renderAttachment(data) : renderTextMessage(data)}</div>
                <div className={classes.date}>{
                    formatISOtoHumanReadable(data.state.timestamp, getDaysFromNow(data.state.timestamp) === 0 ? "h:mm A" : undefined)
                }</div>
            </div>
        </div>
    )

}

export default Message;