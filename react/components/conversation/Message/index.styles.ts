import {makeStyles} from '@mui/styles'

export default makeStyles({
    author: {
        marginRight: '12px',
    },
    avatar: {
        width: '28px',
        height: '28px',
        borderRadius: '50%',
    },
    content: {
        padding: '20px',
        margin: 0,
        maxWidth: '400px',
        overflowWrap: 'break-word',
    },
    date: {
        color: '#000',
        fontSize: '13px',
        lineHeight: '20px',
        textAlign: 'start',
    },
    text: {
        whiteSpace: 'pre-line',
    },
    attachment: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        textDecoration: 'none',
        color: '#000',
    },
    icon: {
        transform: 'rotate(90deg)',
        marginRight: '10px',
        color: '#000'
    },
    filename: {
        fontSize: '14px',
        lineHeight: '20px',
    },
    remoteMessage: {
        gap: '16px',
        marginBottom: '40px',
        justifyContent: 'flex-start',
        alignItems: 'baseline',
    },
    localMessage: {
        gap: '16px',
        marginBottom: '40px',
        justifyContent: 'flex-start',
        flexDirection: 'row-reverse',
        alignItems: 'flex-start',
    },
    messageContent: {
        backgroundColor: "#f05",
        borderRadius: '8px 8px 8px 0',
    }


})