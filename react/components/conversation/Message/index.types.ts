export interface MessageProps {
    data: Message,
    local: User<DoctorType>,
    avatar: Image,
    renderLocalAuthor: boolean;
}