from django.conf import settings
from graphql_jwt.testcases import JSONWebTokenTestCase
from graphql_relay import to_global_id

from api.v2.chat.subscriptions import TEST_MESSAGE_CONTENT
from api.v2.chat.types import ChatGroupType, ChatMessageType
from api.v2.user.types import UserType
from core.celery import app as celery_app
from apps.config.utils import TEST_EMAIL_BACKEND
from apps.users.models import Account
from apps.chat.models import (
    ChatGroup,
    ChatMessage,
    ChatMessageStatus,
    ChatMembership,
)


class ChatTestCase(JSONWebTokenTestCase):

    def setUp(self):
        self.user = Account.objects.create(
            username='Some username',
            first_name='Sergey',
            last_name='Greb',
            email='sergey@gmail.com',
            password='asddsa',
            is_active=True
        )
        self.partner_user = Account.objects.create(
            username='Another username',
            first_name='Max',
            last_name='Maz',
            email='max@gmail.com',
            password='asddsa',
            is_active=True
        )
        self.group = ChatGroup.objects.create(
            group_type=1,
            title="some title"
        )

        # Ignore celery tasks and change default email backend
        celery_app.conf.task_always_eager = True
        settings.EMAIL_BACKEND = TEST_EMAIL_BACKEND

    def test_user_can_create_group_with_type_group(self):
        self.client.authenticate(self.user)

        global_partner_user_id = to_global_id(
            type=UserType._meta.name,
            id=self.partner_user.id
        )
        group_name = "TestChat1"

        mutation = f'''mutation {{
            chatGroupCreate(
                input: {{
                groupType: {ChatGroup.GROUP_TYPES.group},
                groupTitle: "{group_name}",
                accountIds: ["{global_partner_user_id}"],
                }}
            ) {{
            success
            errors
            chatGroup{{
                id
                title
            }}
          }}
        }}'''
        response = self.client.execute(mutation)
        content = response.data

        self.assertEqual(content['chatGroupCreate']['success'], 1)
        self.assertEqual(
            content['chatGroupCreate']['chatGroup']['title'],
            group_name
        )

    def test_user_can_update_chat_group_title(self):
        self.client.authenticate(self.user)

        global_group_id = to_global_id(
            type=ChatGroupType._meta.name,
            id=self.group.id
        )
        group_title = 'newGroupTitle'
        mutation = f'''mutation {{
           chatGroupTitleUpdate(
                input: {{
                    groupId: "{global_group_id}",
                    groupTitle: "{group_title}"
                }}
           ) {{
                chatGroup {{
                    id
                    title
                }}
                success
                errors
            }}
         }}'''

        response = self.client.execute(mutation)
        content = response.data

        self.assertEqual(content['chatGroupTitleUpdate']['success'], 1)
        self.assertEqual(
            content['chatGroupTitleUpdate']['chatGroup']['title'],
            group_title
        )

    def test_user_can_not_update_chat_group_title(self):
        self.client.authenticate(self.user)

        group = ChatGroup.objects.create(
            group_type=ChatGroup.GROUP_TYPES.private,
            title="some title"
        )

        global_group_id = to_global_id(
            type=ChatGroupType._meta.name,
            id=group.id
        )
        group_title = 'newGroupTitle'

        mutation = f'''mutation {{
           chatGroupTitleUpdate(
             input: {{
               groupId: "{global_group_id}",
               groupTitle: "{group_title}"
            }}
           ) {{
                chatGroup {{
                    id
                    title
                }}
                success
                errors
           }}
         }}'''

        response = self.client.execute(mutation)
        content = response.data

        self.assertEqual(content['chatGroupTitleUpdate']['success'], False)
        self.assertEqual(
            content['chatGroupTitleUpdate']['errors'][0],
            f'Only chats with type group can be updated with '
            f'new title. Yo have provided the chat with '
            f'type {ChatGroup.GROUP_TYPES[group.group_type]}'
        )

    def test_user_can_create_membership(self):
        self.client.authenticate(self.user)

        global_partner_user_id = to_global_id(
            type=UserType._meta.name,
            id=self.partner_user.id
        )
        test_group = ChatGroup.objects.create(
            group_type=1,
        )
        global_group_id = to_global_id(
            type=ChatGroupType._meta.name,
            id=test_group.id
        )
        mutation = f'''mutation {{
          chatMembershipCreate(
            input: {{
              groupId: "{global_group_id}",
              accountId: "{global_partner_user_id}"
            }}
          ) {{
            success
            errors
          }}
        }}'''

        response = self.client.execute(mutation)
        content = response.data

        self.assertEqual(content['chatMembershipCreate']['success'], 1)

    def test_user_can_not_create_chatmembership_if_it_exists(self):
        self.client.authenticate(self.user)

        global_partner_user_id = to_global_id(
            type=UserType._meta.name,
            id=self.partner_user.id
        )
        test_group = ChatGroup.objects.create(
            group_type=1,
        )
        global_group_id = to_global_id(
            type=ChatGroupType._meta.name,
            id=test_group.id
        )
        ChatMembership.objects.create(
            account=self.partner_user,
            group=test_group
        )

        mutation = f'''mutation {{
          chatMembershipCreate(
            input: {{
              groupId: "{global_group_id}",
              accountId: "{global_partner_user_id}"
            }}
          ) {{
            success
            errors
          }}
        }}'''

        response = self.client.execute(mutation)
        content = response.data

        self.assertEqual(content['chatMembershipCreate']['success'], False)
        self.assertEqual(
            content['chatMembershipCreate']['errors'][0],
            "The account is already joined the chat group"
        )

    def test_user_can_leave_group(self):
        self.client.authenticate(self.user)

        ChatMembership.objects.create(
            account=self.user,
            group=self.group
        )
        global_group_id = to_global_id(
            type=ChatGroupType._meta.name,
            id=self.group.id
        )
        mutation = f'''mutation {{
          chatGroupLeave (
            input: {{
                groupId: "{global_group_id}"
            }}
          )
          {{
            success
          }}
        }}
        '''

        response = self.client.execute(mutation)
        content = response.data

        self.assertEqual(content['chatGroupLeave']['success'], 1)

    def test_user_can_send_message(self):
        self.client.authenticate(self.user)

        ChatMembership.objects.create(
            account=self.user,
            group=self.group
        )
        global_group_id = to_global_id(
            type=ChatGroupType._meta.name,
            id=self.group.id
        )

        mutation = f'''mutation {{
          chatMessageSend(
            input: {{
                content: "{TEST_MESSAGE_CONTENT}",
                groupId: "{global_group_id}"
            }}
          ) {{
            success
                message{{
                    id
                    content
                    isEdited
                    isDeleted
                }}
          }}
        }}'''

        response = self.client.execute(mutation)
        content = response.data

        self.assertEqual(content['chatMessageSend']['success'], 1)
        self.assertEqual(
            content['chatMessageSend']['message']['content'],
            TEST_MESSAGE_CONTENT
        )

    def test_user_can_not_send_message_if_no_content_or_attachments(self):
        self.client.authenticate(self.user)

        ChatMembership.objects.create(
            account=self.user,
            group=self.group
        )
        global_group_id = to_global_id(
            type=ChatGroupType._meta.name,
            id=self.group.id
        )

        mutation = f'''mutation {{
          chatMessageSend(
            input: {{
                groupId: "{global_group_id}"
            }}
          ) {{
            success
            errors
                message{{
                    id
                    content
                    isEdited
                    isDeleted
                }}
          }}
        }}'''

        response = self.client.execute(mutation)
        content = response.data

        self.assertEqual(content['chatMessageSend']['success'], False)

        self.assertEqual(
            content['chatMessageSend']['errors'][0],
            'The request should contain message content, attachments or both.'
        )

    def test_user_can_not_send_message_if_not_in_group(self):
        self.client.authenticate(self.user)

        chat = ChatMembership.objects.create(
            account=self.user,
            group=self.group
        )
        global_group_id = to_global_id(
            type=ChatGroupType._meta.name,
            id=self.group.id
        )
        chat.delete()
        mutation = f'''mutation {{
          chatMessageSend(
            input: {{
                content: "{TEST_MESSAGE_CONTENT}",
                groupId: "{global_group_id}"
            }}
          ) {{
            success
            errors
                message{{
                    id
                    content
                    isEdited
                    isDeleted
                }}
          }}
        }}'''

        response = self.client.execute(mutation)
        content = response.data

        self.assertEqual(content['chatMessageSend']['success'], False)

        self.assertEqual(
            content['chatMessageSend']['errors'][0],
            'You are not invited to the group some title'
        )

    def test_user_can_edit_message(self):
        self.client.authenticate(self.user)

        ChatMembership.objects.create(
            account=self.user,
            group=self.group
        )
        message = ChatMessage.objects.create(
            account=self.user,
            group=self.group,
            content='Test message',
            is_edited=False,
            is_deleted=False
        )
        global_message_id = to_global_id(
            type=ChatMessageType._meta.name,
            id=message.id
        )
        new_message_content = "Test message New"
        mutation = f'''mutation {{
            chatMessageEdit(
                input: {{
                    messageId: "{global_message_id}",
                    content: "{new_message_content}"
                }}
            ) {{
            success
          }}
        }}'''

        response = self.client.execute(mutation)
        content = response.data

        self.assertEqual(content['chatMessageEdit']['success'], 1)

    def test_user_can_delete_message(self):
        self.client.authenticate(self.user)

        ChatMembership.objects.create(
            account=self.user,
            group=self.group
        )
        message = ChatMessage.objects.create(
            account=self.user,
            group=self.group,
            content='Test message',
            is_edited=False,
            is_deleted=False

        )
        global_message_id = to_global_id(
            type=ChatMessageType._meta.name,
            id=message.id
        )
        mutation = f'''mutation {{
            chatMessageDelete(
                input: {{
                    messageId: "{global_message_id}"
                }}
            ) {{
            success
            }}
        }}'''

        response = self.client.execute(mutation)
        content = response.data

        self.assertEqual(content['chatMessageDelete']['success'], 1)

    def test_user_can_mark_message_as_read(self):
        self.client.authenticate(self.user)

        ChatMembership.objects.create(
            account=self.user,
            group=self.group
        )
        message = ChatMessage.objects.create(
            account=self.user,
            group=self.group,
            content='Test message',
            is_edited=False,
            is_deleted=False

        )
        ChatMessageStatus.objects.create(
            account=self.user,
            message=message,
            is_read=False
        )
        global_message_id = to_global_id(
            type=ChatMessageType._meta.name,
            id=message.id
        )
        global_user_id = to_global_id(
            type=UserType._meta.name,
            id=self.user.id
        )
        mutation = f'''mutation {{
            chatMessageMarkRead(
                input: {{
                    messageId: "{global_message_id}",
                    accountId: "{global_user_id}"
                }}
            ) {{
                success
                unreadMessageCount
                message {{
                    id
                }}
                group {{
                    id
                }}
            }}
        }}'''

        response = self.client.execute(mutation)
        content = response.data

        self.assertEqual(content['chatMessageMarkRead']['success'], 1)
        self.assertEqual(
            content['chatMessageMarkRead']['unreadMessageCount'], 0
        )

    def test_user_can_list_groups(self):
        self.client.authenticate(self.user)

        ChatMembership.objects.create(
            account=self.user,
            group=self.group
        )

        message = ChatMessage.objects.create(
            account=self.user,
            group=self.group,
            content='Test message',
            is_edited=False,
            is_deleted=False

        )
        self.group.last_message = message
        self.group.save()

        query = '''{
        chatGroupList {
            totalCount
            edges {
                node {
                    id
                    title
                    groupType
                    lastMessage {
                        id
                        content
                        createdAt
                        attachments {
                            id
                        }
                    }
                    createdAt
                    members {
                        id
                    }
                }
            }
          }
        }
        '''

        response = self.client.execute(query)
        content = response.data

        self.assertEqual(content['chatGroupList']['totalCount'], 1)

    def test_user_can_search_groups(self):
        self.client.authenticate(self.user)

        group_name = 'Some cool chat'

        test_group = ChatGroup.objects.create(
            group_type=1,
            title=group_name + ' it is',
        )
        ChatMembership.objects.create(
            account=self.user,
            group=test_group
        )
        message = ChatMessage.objects.create(
            account=self.user,
            group=test_group,
            content='Test message',
            is_edited=False,
            is_deleted=False

        )
        test_group.last_message = message
        test_group.save()

        query = f'''{{
            chatGroupList (search: "{group_name}"){{
                totalCount
                edges {{
                    node {{
                        id
                        title
                    }}
                }}
            }}
        }}'''

        response = self.client.execute(query)
        content = response.data

        self.assertEqual(content['chatGroupList']['totalCount'], 1)

    def test_user_can_get_group_details(self):
        self.client.authenticate(self.user)

        global_user_id = to_global_id(
            type=UserType._meta.name,
            id=self.user.id
        )
        global_group_id = to_global_id(
            type=ChatGroupType._meta.name,
            id=self.group.id
        )
        ChatMembership.objects.create(
            account=self.user,
            group=self.group
        )
        query = f'''{{
            chatGroupDetails (
                id: "{global_group_id}"
            ) {{
                id
                title
                groupType
                    members {{
                        id
                    }}
                }}
        }}'''

        response = self.client.execute(query)
        content = response.data

        self.assertEqual(content['chatGroupDetails']['id'], global_group_id)
        self.assertEqual(
            content['chatGroupDetails']['members'][0]['id'],
            global_user_id
        )

    def test_user_can_list_messages(self):
        self.client.authenticate(self.user)

        ChatMembership.objects.create(
            account=self.user,
            group=self.group
        )
        ChatMessage.objects.create(
            account=self.user,
            group=self.group,
            content='Test message',
            is_edited=False,
            is_deleted=False
        )

        query = '''{
            chatMessageList (ordering: "-created_at"){
                totalCount
                edges {
                    node {
                        id
                        createdAt
                        isRead
                        content
                        isEdited
                        isDeleted
                        attachments {
                            filename
                            url
                            size
                            mimetype
                        }
                        account {
                            id
                        }
                        group {
                            id
                        }
                    }
                }
            }
        }'''

        response = self.client.execute(query)
        content = response.data

        self.assertEqual(content['chatMessageList']['totalCount'], 1)

    def test_user_can_filter_list_messages_by_group(self):
        self.client.authenticate(self.user)

        group2 = ChatGroup.objects.create(
            group_type=1,
            title="some title"
        )
        ChatMembership.objects.create(
            account=self.user,
            group=group2
        )
        ChatMessage.objects.create(
            account=self.user,
            group=self.group,
            content='Test message',
            is_edited=False,
            is_deleted=False
        )
        global_group_id = to_global_id(
            type=ChatGroupType._meta.name,
            id=self.group.id
        )

        query = f'''{{
            chatMessageList (group: "{global_group_id}"){{
                totalCount
                edges {{
                    node {{
                        id
                        createdAt
                        isRead
                        content
                        isEdited
                        isDeleted
                        attachments {{
                            filename
                            url
                            size
                            mimetype
                        }}
                        account {{
                            id
                        }}
                        group {{
                            id
                        }}
                    }}
                }}
            }}
        }}'''

        response = self.client.execute(query)
        content = response.data

        self.assertEqual(content['chatMessageList']['totalCount'], 1)

    def test_user_can_filtes_list_messages_by_content(self):
        self.client.authenticate(self.user)

        ChatMessage.objects.create(
            account=self.user,
            group=self.group,
            content='Another message',
            is_edited=False,
            is_deleted=False
        )
        ChatMessage.objects.create(
            account=self.user,
            group=self.group,
            content='Some message',
            is_edited=False,
            is_deleted=False
        )

        query = '''{
            chatMessageList (search: "Another"){
                totalCount
                edges {
                    node {
                        id
                        createdAt
                        isRead
                        content
                        isEdited
                        isDeleted
                        attachments {
                            filename
                            url
                            size
                            mimetype
                        }
                        account {
                            id
                        }
                        group {
                            id
                        }
                    }
                }
            }
        }'''

        response = self.client.execute(query)
        content = response.data

        self.assertEqual(content['chatMessageList']['totalCount'], 1)

    def test_user_can_get_ordering_list_messages_by_created_at(self):
        self.client.authenticate(self.user)

        ChatMessage.objects.create(
            account=self.user,
            group=self.group,
            content='Another message',
            is_edited=False,
            is_deleted=False
        )
        ChatMessage.objects.create(
            account=self.user,
            group=self.group,
            content='Some message',
            is_edited=False,
            is_deleted=False
        )

        query = '''{
            chatMessageList (ordering: "-created_at"){
                totalCount
                edges {
                    node {
                        id
                        createdAt
                        isRead
                        content
                        isEdited
                        isDeleted
                        attachments {
                            filename
                            url
                            size
                            mimetype
                        }
                        account {
                            id
                        }
                        group {
                            id
                        }
                    }
                }
            }
        }'''

        response = self.client.execute(query)
        content = response.data

        self.assertEqual(content['chatMessageList']['totalCount'], 2)

    def test_user_can_get_list_of_unread_messages(self):
        self.client.authenticate(self.user)

        message1 = ChatMessage.objects.create(
            account=self.user,
            group=self.group,
            content='Another message',
            is_edited=False,
            is_deleted=False
        )

        message2 = ChatMessage.objects.create(
            account=self.user,
            group=self.group,
            content='Some message',
            is_edited=False,
            is_deleted=False
        )
        ChatMessageStatus.objects.create(
            account=self.user,
            message=message1,
            is_read=True
        )
        ChatMessageStatus.objects.create(
            account=self.user,
            message=message2,
            is_read=False
        )
        query = '''{
            chatUnreadMessagesCount
        }'''

        response = self.client.execute(query)
        content = response.data

        self.assertEqual(
            content['chatUnreadMessagesCount'], 1
        )
