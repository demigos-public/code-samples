import graphene
import magic
from django.db.models import Q
from graphene import relay
from graphene_file_upload.scalars import Upload
from graphql_jwt.decorators import login_required, user_passes_test

from graphql_relay.node.node import from_global_id, to_global_id

from api.v2.chat.subscriptions import OnNewChatMessage
from api.v2.chat.types import (
    ChatGroupType,
    ChatMessageType,
    ChatMembershipType
)
from apps.users.tasks import send_mobile_push_notification
from api.v2.user.types import UserType
from apps.chat.models import (
    ChatGroup,
    ChatMessage,
    ChatMessageAttachment,
    ChatMessageStatus,
    ChatMembership
)
from apps.users.models import Account


class ChatMessageSend(relay.ClientIDMutation):
    """
    Mutation for chat message create
    """

    class Input:
        group_id = graphene.ID(required=True)
        content = graphene.String()
        attachments = Upload()

    message = graphene.Field(ChatMessageType)
    success = graphene.Boolean()
    errors = graphene.List(graphene.String)

    @login_required
    def mutate_and_get_payload(root, info, **input):
        errors = []

        attachments = input.get('attachments')
        content = input.get('content')

        if not attachments and not content:
            errors.append(
                'The request should contain message content, '
                'attachments or both.'
            )
            return ChatMessageSend(
                message=None,
                success=False,
                errors=errors
            )

        global_group_id = input.get('group_id')
        group_id = from_global_id(global_group_id)[1]
        group = ChatGroup.objects.get(id=group_id)

        account = info.context.user

        try:
            ChatMembership.objects.get(
                account=account,
                group=group
            )
        except ChatMembership.DoesNotExist:
            errors.append(f'You are not invited to the group {group.title}')
            return ChatMessageSend(
                message=None,
                success=False,
                errors=errors
            )
        blocked_membership = ChatMembership.objects.filter(
            is_blocked=True,
            group=group
        )
        if blocked_membership:
            errors.append(f'You are not able to send messages. '
                          f'This chat was blocked by '
                          f'{blocked_membership.first().account.name}')
            return ChatMessageSend(
                message=None,
                success=False,
                errors=errors
            )
        message = ChatMessage.objects.create(
            group=group,
            account=account,
            content=content
        )

        if attachments:
            if not isinstance(attachments, list):
                attachments = [attachments]
            for file in attachments:
                ChatMessageAttachment.objects.create(
                    message=message,
                    file=file
                )
        group.last_message = message
        group.updated_at = message.created_at
        group.save()

        for each in ChatMembership.objects.filter(group=group):
            # MessageStatus should be is_read == True for the current account
            # and False for other chat members
            is_read = each.account == account
            ChatMessageStatus.objects.create(
                account=each.account,
                message=message,
                is_read=is_read
            )
            # is_read as a flag of the sender
            if not each.is_muted and not is_read:
                content = 'File(s)' if not content and attachments else content
                if group.group_type == ChatGroup.GROUP_TYPES.group:
                    message_title = group.title
                    message_body = f'{account.name}: {content}'
                else:
                    message_title = account.name
                    message_body = content
                send_mobile_push_notification.delay(
                    user_pk=each.account.pk,
                    title=message_title,
                    body=message_body,
                    chatId=global_group_id,
                    muted=each.is_muted
                )

        # Notify subscribers.
        OnNewChatMessage.new_chat_message(
            message=message
        )
        success = True if not errors else False
        return ChatMessageSend(
            message=message,
            success=success,
            errors=errors
        )


class ChatMembershipCreate(relay.ClientIDMutation):
    """
    Mutation for chat group and membership create
    """

    class Input:
        group_id = graphene.ID(required=True)
        account_id = graphene.ID(required=True)

    success = graphene.Boolean()
    errors = graphene.List(graphene.String)
    chat_membership = graphene.Field(ChatMembershipType)

    @login_required
    def mutate_and_get_payload(root, info, **input):
        errors = []
        group_id = from_global_id(input.get('group_id'))[1]
        group = ChatGroup.objects.get(id=group_id)
        account_id = from_global_id(input.get('account_id'))[1]
        account = Account.objects.get(id=account_id)
        try:
            existing_membership = ChatMembership.objects.get(
                account=account,
                group=group
            )
            errors.append('The account is already joined the chat group')
            return ChatMembershipCreate(
                chat_membership=existing_membership,
                success=False,
                errors=errors
            )
        except ChatMembership.DoesNotExist:
            pass

        chat_membership = ChatMembership.objects.create(
            account=account,
            group=group
        )

        success = True if not errors else False
        return ChatMembershipCreate(
            chat_membership=chat_membership,
            success=success,
            errors=errors
        )


class ChatGroupCreate(relay.ClientIDMutation):
    """
    Mutation for chat group and membership create
    """

    class Input:
        group_type = graphene.Int(required=True)
        account_ids = graphene.List(graphene.ID)
        group_title = graphene.String()
        avatar = Upload()

    chat_group = graphene.Field(ChatGroupType)
    success = graphene.Boolean()
    errors = graphene.List(graphene.String)

    @login_required
    def mutate_and_get_payload(root, info, **input):
        errors = []

        group_type = input.get('group_type')
        avatar = input.get('avatar')
        group_title = input.get('group_title')
        account_ids = input.get('account_ids') if input.get(
            'account_ids') else []
        current_account_id = to_global_id(
            type=UserType._meta.name,
            id=info.context.user.id
        )
        if (group_type == ChatGroup.GROUP_TYPES.private
                and current_account_id in account_ids):
            return ChatGroupCreate(
                chat_group=None,
                success=False,
                errors=['Can not create private chat with yourself']
            )

        account_ids.append(current_account_id)
        account_ids = set(account_ids)

        account_list = []
        for each_account_id in account_ids:
            try:
                each_global_id = from_global_id(each_account_id)[1]
                if not each_global_id:
                    continue
                account_list.append(Account.objects.get(id=each_global_id))
            except Account.DoesNotExist:
                errors.append(
                    f'Account with ID: {each_account_id} was not added'
                    f'to the group because there is no such account')

        if group_type == ChatGroup.GROUP_TYPES.group:
            if not group_title:
                return ChatGroupCreate(
                    chat_group=None,
                    success=False,
                    errors=['Group title should be provided in case of not '
                            'private chat group creation']
                )
        else:
            if len(account_list) != 2:
                return ChatGroupCreate(
                    chat_group=None,
                    success=False,
                    errors=[
                        'Group with type private requires accountID '
                        'of the chat companion'
                    ]
                )
            group_title = ' '.join([each.name for each in account_list])

        if group_type == ChatGroup.GROUP_TYPES.group:
            condition = Q(title__icontains=group_title)
        else:
            condition = Q(title__icontains=account_list[0].name)
            condition &= Q(title__icontains=account_list[1].name)

        existing_group = ChatGroup.objects.filter(
            Q(group_type=group_type) & condition
        ).first()
        if existing_group:
            return ChatGroupCreate(
                chat_group=existing_group,
                success=True,
                errors=errors
            )

        if avatar:
            content_type = magic.from_buffer(
                avatar.read(),
                mime=True
            ).split('/')[0]
            if content_type != 'image':
                return ChatGroupCreate(
                    chat_group=None,
                    success=False,
                    errors=['Chat avatar should be an image mimetype']
                )

        group = ChatGroup.objects.create(
            title=group_title,
            group_type=group_type,
            avatar=avatar
        )
        global_group_id = to_global_id(
            type=ChatGroupType._meta.name,
            id=group.id
        )
        for each_account in account_list:
            ChatMembership.objects.create(
                group=group,
                account=each_account
            )
            if each_account != info.context.user:
                send_mobile_push_notification.delay(
                    user_pk=each_account.pk,
                    title=group.title,
                    body='You have been added to this chat',
                    chatId=global_group_id
                )

        success = True if not errors else False
        return ChatGroupCreate(
            chat_group=group,
            success=success,
            errors=errors
        )


class ChatPersonalAnnouncementCreate(relay.ClientIDMutation):
    """
    Mutation that send to users private chat
    (and creates it if id does not exist) group announcement
    """

    class Input:
        content = graphene.String(required=True)
        account_ids = graphene.List(graphene.ID, required=True)

    success = graphene.Boolean()
    errors = graphene.List(graphene.String)

    @user_passes_test(lambda u: u.is_superuser)
    def mutate_and_get_payload(root, info, **input):
        errors = []
        account_ids = input.get('account_ids')
        content = input.get('content')
        current_account = info.context.user
        current_account_id = to_global_id(
            type=UserType._meta.name,
            id=info.context.user.id
        )
        if current_account_id in account_ids:
            return ChatGroupCreate(
                chat_group=None,
                success=False,
                errors=['Can not create private chat with yourself']
            )
        account_ids = set(account_ids)
        for each_account_id in account_ids:
            each_id = from_global_id(each_account_id)[1]
            if not each_id:
                continue
            each_account = Account.objects.get(id=each_id)
            condition = Q(title__icontains=current_account.name)
            condition &= Q(title__icontains=each_account.name)
            existing_group = ChatGroup.objects.filter(
                Q(group_type=ChatGroup.GROUP_TYPES.private) & condition
            ).first()
            if not existing_group:
                group_title = f'{each_account.name} {current_account.name}'
                existing_group = ChatGroup.objects.create(
                    group_type=ChatGroup.GROUP_TYPES.private,
                    title=group_title
                )
            global_group_id = to_global_id(
                type=ChatGroupType._meta.name,
                id=existing_group.id
            )
            ChatMembership.objects.get_or_create(
                group=existing_group,
                account=each_account
            )
            ChatMembership.objects.get_or_create(
                group=existing_group,
                account=current_account
            )
            message = ChatMessage.objects.create(
                group=existing_group,
                account=current_account,
                content=content
            )
            ChatMessageStatus.objects.create(
                account=each_account,
                message=message,
                is_read=False
            )
            ChatMessageStatus.objects.create(
                account=current_account,
                message=message,
                is_read=True
            )
            existing_group.updated_at = message.created_at
            existing_group.last_message = message
            existing_group.save()
            send_mobile_push_notification.delay(
                user_pk=each_account.pk,
                title=current_account.name,
                body=content,
                chatId=global_group_id
            )
            # Notify subscribers.
            OnNewChatMessage.new_chat_message(
                message=message
            )
        success = True if not errors else False
        return ChatPersonalAnnouncementCreate(
            success=success,
            errors=errors
        )


class ChatGroupTitleUpdate(relay.ClientIDMutation):
    """
    Mutation for chat group title update
    """

    class Input:
        group_id = graphene.ID(required=True)
        group_title = graphene.String(required=True)

    chat_group = graphene.Field(ChatGroupType)
    success = graphene.Boolean()
    errors = graphene.List(graphene.String)

    @login_required
    def mutate_and_get_payload(root, info, **input):
        errors = []

        group_id = from_global_id(input.get('group_id'))[1]
        group_title = input.get('group_title')

        group = ChatGroup.objects.get(id=group_id)

        if group.group_type != ChatGroup.GROUP_TYPES.group:
            errors.append(f'Only chats with type group can be updated with '
                          f'new title. Yo have provided the chat with '
                          f'type {ChatGroup.GROUP_TYPES[group.group_type]}')
            return ChatMessageSend(
                message=None,
                success=False,
                errors=errors
            )

        group.title = group_title
        group.save()

        success = True if not errors else False
        return ChatGroupTitleUpdate(
            chat_group=group,
            success=success,
            errors=errors
        )


class ChatMessageUpdate(relay.ClientIDMutation):
    """
    Mutation for edit chat message
    """

    class Input:
        message_id = graphene.ID(required=True)
        content = graphene.String(required=True)

    success = graphene.Boolean()
    message = graphene.Field(ChatMessageType)
    errors = graphene.List(graphene.String)

    @login_required
    def mutate_and_get_payload(root, info, **input):
        errors = []

        message_id = from_global_id(input.get('message_id'))[1]
        message = ChatMessage.objects.get(id=message_id)

        message.is_edited = True
        message.content = input.get('content')
        message.save()

        success = True if not errors else False
        return ChatMessageUpdate(
            message=message,
            success=success,
            errors=errors
        )


class ChatMessageMarkRead(relay.ClientIDMutation):
    """
    Mutation for mark chat message as read
    """

    class Input:
        message_id = graphene.ID(required=True)
        account_id = graphene.ID(required=True)

    group = graphene.Field(ChatGroupType)
    message = graphene.Field(ChatMessageType)
    unread_message_count = graphene.Int()
    success = graphene.Boolean()
    errors = graphene.List(graphene.String)

    @login_required
    def mutate_and_get_payload(root, info, **input):
        errors = []

        account_id = from_global_id(input.get('account_id'))[1]
        account = Account.objects.get(id=account_id)
        message_id = from_global_id(input.get('message_id'))[1]
        message = ChatMessage.objects.get(id=message_id)

        message_status = ChatMessageStatus.objects.get(
            account=account,
            message=message
        )

        message_status.is_read = True
        message_status.save()

        count = ChatMessageStatus.objects.filter(
            account=info.context.user,
            is_read=False
        ).count()

        success = True if not errors else False
        return ChatMessageMarkRead(
            unread_message_count=count,
            group=message.group,
            message=message,
            success=success,
            errors=errors
        )


class ChatMessageDelete(relay.ClientIDMutation):
    """
    Mutation for delete chat message
    """

    class Input:
        message_id = graphene.ID(required=True)

    success = graphene.Boolean()
    message = graphene.Field(ChatMessageType)
    errors = graphene.List(graphene.String)

    @login_required
    def mutate_and_get_payload(root, info, **input):
        errors = []

        message_id = from_global_id(input.get('message_id'))[1]
        message = ChatMessage.objects.get(id=message_id)

        message.is_deleted = True
        message.save()

        success = True if not errors else False
        return ChatMessageUpdate(
            message=message,
            success=success,
            errors=errors
        )


class ChatGroupLeave(relay.ClientIDMutation):
    """
    Mutation for leaving chat group
    """

    class Input:
        group_id = graphene.ID(required=True)

    success = graphene.Boolean()
    errors = graphene.List(graphene.String)
    chat_group = graphene.Field(ChatGroupType)

    @login_required
    def mutate_and_get_payload(root, info, **input):
        errors = []
        group_id = from_global_id(input.get('group_id'))[1]
        group = ChatGroup.objects.get(id=group_id)
        account = info.context.user
        ChatMembership.objects.get(account=account, group=group).delete()

        success = True if not errors else False
        return ChatGroupLeave(
            chat_group=group,
            success=success,
            errors=errors
        )


class ChatGroupMemberDelete(relay.ClientIDMutation):
    """
    Mutation for kicking member from chat group
    """

    class Input:
        group_id = graphene.ID(required=True)
        account_id = graphene.ID(required=True)

    success = graphene.Boolean()
    errors = graphene.List(graphene.String)
    chat_group = graphene.Field(ChatGroupType)

    @login_required
    def mutate_and_get_payload(root, info, **input):
        errors = []
        group_id = from_global_id(input.get('group_id'))[1]
        group = ChatGroup.objects.get(id=group_id)
        account_id = from_global_id(input.get('account_id'))[1]
        account = Account.objects.get(id=account_id)
        try:
            existing_membership = ChatMembership.objects.get(
                account=account,
                group=group
            )
        except ChatMembership.DoesNotExist:
            errors.append(f'The account is not in this chat group {group}')
            return ChatGroupMemberDelete(
                chat_group=group,
                success=False,
                errors=errors
            )

        existing_membership.delete()
        messages = ChatMessage.objects.filter(group=group)
        for each in ChatMessageStatus.objects.filter(
                account=account, message__in=messages):
            each.delete()

        success = True if not errors else False
        return ChatGroupMemberDelete(
            chat_group=group,
            success=success,
            errors=errors
        )


class ChatGroupNotificationsMute(relay.ClientIDMutation):
    """
    Mutation for muting notifications in group
    """

    class Input:
        group_id = graphene.ID(required=True)

    success = graphene.Boolean()
    errors = graphene.List(graphene.String)

    @login_required
    def mutate_and_get_payload(root, info, **input):
        group_id = from_global_id(input.get('group_id'))[1]
        group = ChatGroup.objects.get(id=group_id)
        account = info.context.user
        membership = ChatMembership.objects.get(account=account, group=group)
        if membership:
            membership.is_muted = True
            membership.save()
        return ChatGroupNotificationsMute(success=True)


class ChatGroupNotificationsUnmute(relay.ClientIDMutation):
    """
    Mutation for unmuting notifications in group
    """

    class Input:
        group_id = graphene.ID(required=True)

    success = graphene.Boolean()
    errors = graphene.List(graphene.String)

    @login_required
    def mutate_and_get_payload(root, info, **input):
        group_id = from_global_id(input.get('group_id'))[1]
        group = ChatGroup.objects.get(id=group_id)
        account = info.context.user
        membership = ChatMembership.objects.get(account=account, group=group)
        if membership:
            membership.is_muted = False
            membership.save()
        return ChatGroupNotificationsUnmute(success=True)


class ChatGroupBlock(relay.ClientIDMutation):
    """
    Mutation for blocking group
    """

    class Input:
        group_id = graphene.ID(required=True)

    success = graphene.Boolean()
    errors = graphene.List(graphene.String)

    @login_required
    def mutate_and_get_payload(root, info, **input):
        errors = []
        group_id = from_global_id(input.get('group_id'))[1]
        group = ChatGroup.objects.get(id=group_id)
        if group.group_type == ChatGroup.GROUP_TYPES.group:
            errors.append(
                'The group chat cannot be blocked'
            )
            return ChatMessageSend(
                message=None,
                success=False,
                errors=errors
            )
        account = info.context.user
        membership = ChatMembership.objects.get(account=account, group=group)
        if membership:
            membership.is_blocked = True
            membership.save()
        return ChatGroupBlock(success=True)


class ChatGroupUnblock(relay.ClientIDMutation):
    """
    Mutation for unblocking group
    """

    class Input:
        group_id = graphene.ID(required=True)

    success = graphene.Boolean()
    errors = graphene.List(graphene.String)

    @login_required
    def mutate_and_get_payload(root, info, **input):
        group_id = from_global_id(input.get('group_id'))[1]
        group = ChatGroup.objects.get(id=group_id)
        account = info.context.user
        membership = ChatMembership.objects.get(account=account, group=group)
        if membership:
            membership.is_blocked = False
            membership.save()
        return ChatGroupUnblock(success=True)
