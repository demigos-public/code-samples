import PIL
import graphene
import magic
from graphene import relay
from graphene_django.types import DjangoObjectType
from graphql_jwt.decorators import login_required

from api.v2.chat import sizeof_fmt
from api.v2.config.connections import CountableConnection

from api.v2.user.types import UserType
from apps.chat.models import (
    ChatGroup,
    ChatMessage,
    ChatMessageAttachment,
    ChatMessageStatus,
    ChatMembership
)
from apps.users.models import Account, ROLE_MODEL_MAPPING, STAFF

IMAGE_MIMETYPE = 'image'


class ChatMessageAttachmentType(DjangoObjectType):
    filename = graphene.String()
    mimetype = graphene.String()
    url = graphene.String()
    size = graphene.String()
    width = graphene.Int()
    height = graphene.Int()

    class Meta:
        model = ChatMessageAttachment
        interfaces = (relay.Node,)
        connection_class = CountableConnection
        fields = (
            'message',
            'filename',
            'mimetype',
            'url',
            'width',
            'height',
            'size'
        )

    @login_required
    def resolve_filename(self, info, **kwargs):
        if self.file:
            return self.file.name

    @login_required
    def resolve_url(self, info, **kwargs):
        if self.file:
            return self.file.url

    @login_required
    def resolve_mimetype(self, info, **kwargs):
        if not self.file:
            return
        self.file.seek(0)
        mimetype = magic.from_buffer(self.file.read(), mime=True)
        return mimetype

    @login_required
    def resolve_width(self, info, **kwargs):
        if not self.file:
            return
        self.file.seek(0)
        mimetype = magic.from_buffer(self.file.read(), mime=True)
        if IMAGE_MIMETYPE in mimetype:
            image = PIL.Image.open(self.file)
            width, height = image.size
            return width

    @login_required
    def resolve_height(self, info, **kwargs):
        if not self.file:
            return
        self.file.seek(0)
        mimetype = magic.from_buffer(self.file.read(), mime=True)
        if IMAGE_MIMETYPE in mimetype:
            image = PIL.Image.open(self.file)
            width, height = image.size
            return height

    @login_required
    def resolve_size(self, info, **kwargs):
        if self.file:
            return sizeof_fmt(self.file.size)


class ChatLastMessageType(DjangoObjectType):
    attachments = graphene.List(ChatMessageAttachmentType)

    class Meta:
        model = ChatMessage
        interfaces = (relay.Node,)
        connection_class = CountableConnection
        fields = (
            'account',
            'content',
            'is_edited',
            'is_deleted',
            'created_at',
            'attachments'
        )

    @login_required
    def resolve_attachments(self, info, **kwargs):
        return ChatMessageAttachment.objects.filter(message=self)


class ChatGroupType(DjangoObjectType):
    title = graphene.String()
    group_type = graphene.String()
    members = graphene.List(UserType)
    avatar_url = graphene.String()
    unread_message_count = graphene.Int()
    is_muted = graphene.Boolean()
    is_block_available = graphene.Boolean()
    is_blocked = graphene.Boolean()
    is_blocked_by = graphene.Field(UserType)

    class Meta:
        model = ChatGroup
        interfaces = (relay.Node,)
        connection_class = CountableConnection
        fields = (
            'title',
            'group_type',
            'last_message',
            'created_at',
            'updated_at',
            'members'
        )

    @login_required
    def resolve_is_muted(self, info, **kwargs):
        account = info.context.user
        membership = ChatMembership.objects.filter(
            group=self, account=account
        ).first()
        return membership.is_muted if membership else False

    @login_required
    def resolve_is_block_available(self, info, **kwargs):
        if self.group_type == ChatGroup.GROUP_TYPES.group:
            return False
        for membership in ChatMembership.objects.filter(group=self):
            if membership.account.type == STAFF:
                return False
        return True

    @login_required
    def resolve_is_blocked(self, info, **kwargs):
        for membership in ChatMembership.objects.filter(group=self):
            if membership.is_blocked:
                return True
        return False

    @login_required
    def resolve_is_blocked_by(self, info, **kwargs):
        for membership in ChatMembership.objects.filter(group=self):
            if membership.is_blocked:
                return membership.account

    @login_required
    def resolve_title(self, info, **kwargs):
        account = info.context.user
        if self.group_type == ChatGroup.GROUP_TYPES.private:
            chat_membership = ChatMembership.objects.filter(
                group=self).exclude(account=account).first()
            if not chat_membership:
                return None
            account = chat_membership.account
            return account.name
        return self.title

    @login_required
    def resolve_group_type(self, info, **kwargs):
        return ChatGroup.GROUP_TYPES[self.group_type]

    @login_required
    def resolve_members(self, info, **kwargs):
        id_list = [
            each.account.id for each in
            ChatMembership.objects.filter(group=self)
        ]
        return Account.objects.filter(id__in=id_list)

    @login_required
    def resolve_avatar_url(self, info, **kwargs):
        if self.avatar:
            return self.avatar.url
        account = info.context.user
        if self.group_type == ChatGroup.GROUP_TYPES.private:
            chat_membership = ChatMembership.objects.filter(
                group=self).exclude(account=account).first()
            if not chat_membership:
                return None
            account = chat_membership.account
            account_class = ROLE_MODEL_MAPPING.get(account.type)
            if account_class:
                account_model = account_class.objects.get(account=account)
                return account_model.image.url if account_model.image else None

    @login_required
    def resolve_unread_message_count(self, info, **kwargs):
        group_messages = ChatMessage.objects.filter(group=self)
        unred_messages = 0
        if group_messages:
            unred_messages = ChatMessageStatus.objects.filter(
                message__in=group_messages,
                account=info.context.user,
                is_read=False
            ).count()
        return unred_messages


class ChatMembershipType(DjangoObjectType):
    group = graphene.Field(ChatGroupType)
    account = graphene.Field(UserType)

    class Meta:
        model = ChatMembership
        interfaces = (relay.Node,)
        connection_class = CountableConnection
        fields = (
            'group',
            'account',
            'created_at'
        )


class ChatMessageType(DjangoObjectType):
    message_id = graphene.ID()
    group = graphene.Field(ChatGroupType)
    account = graphene.Field(UserType)
    content = graphene.String()
    is_edited = graphene.Boolean()
    is_deleted = graphene.Boolean()
    is_read = graphene.Boolean()
    attachments = graphene.List(ChatMessageAttachmentType)

    class Meta:
        model = ChatMessage
        interfaces = (relay.Node,)
        connection_class = CountableConnection
        fields = (
            'group',
            'account',
            'content',
            'is_read',
            'is_edited',
            'is_deleted',
            'created_at',
            'attachments'
        )

    @classmethod
    @login_required
    def get_queryset(self, queryset, info):
        return queryset

    @login_required
    def resolve_is_read(self, info, **kwargs):
        message_status = ChatMessageStatus.objects.filter(
            message=self, account=info.context.user).first()
        if message_status:
            return message_status.is_read
        return

    @login_required
    def resolve_attachments(self, info, **kwargs):
        return ChatMessageAttachment.objects.filter(message=self)

    def resolve_content(self, info):
        return self.content

    def resolve_is_deleted(self, info):
        return self.is_deleted

    def resolve_is_edited(self, info):
        return self.is_edited

    def resolve_account(self, info):
        return self.account

    def resolve_group(self, info):
        return self.group

    def resolve_message_id(self, info, **kwargs):
        return self.pk


class ChatMessageStatusType(DjangoObjectType):
    account = graphene.Field(UserType)
    message = graphene.Field(ChatMessageType)
    is_read = graphene.Boolean()

    class Meta:
        model = ChatMessageStatus
        interfaces = (relay.Node,)
        connection_class = CountableConnection
        fields = (
            'account',
            'message',
            'is_read'
        )
