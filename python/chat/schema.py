import graphene

from .queries import ChatQuery
from .mutations import (
    ChatGroupCreate,
    ChatGroupBlock,
    ChatGroupMemberDelete, ChatGroupUnblock,
    ChatGroupTitleUpdate,
    ChatMembershipCreate,
    ChatMessageUpdate,
    ChatMessageSend,
    ChatMessageDelete,
    ChatGroupLeave,
    ChatMessageMarkRead,
    ChatGroupNotificationsMute,
    ChatGroupNotificationsUnmute, ChatPersonalAnnouncementCreate
)
from .subscriptions import OnNewChatMessage


class ChatMutations(graphene.ObjectType):
    chat_message_send = ChatMessageSend.Field()
    chat_message_edit = ChatMessageUpdate.Field()
    chat_message_delete = ChatMessageDelete.Field()
    chat_message_mark_read = ChatMessageMarkRead.Field()
    chat_membership_create = ChatMembershipCreate.Field()
    chat_group_leave = ChatGroupLeave.Field()
    chat_group_member_delete = ChatGroupMemberDelete.Field()
    chat_group_create = ChatGroupCreate.Field()
    chat_group_title_update = ChatGroupTitleUpdate.Field()
    chat_group_notifications_mute = ChatGroupNotificationsMute.Field()
    chat_group_notifications_unmute = ChatGroupNotificationsUnmute.Field()
    chat_group_block = ChatGroupBlock.Field()
    chat_group_unblock = ChatGroupUnblock.Field()
    chat_personal_announcement_create = ChatPersonalAnnouncementCreate.Field()


class ChatQueries(ChatQuery):
    pass


class ChatSubscriptions(graphene.ObjectType):
    on_new_chat_message = OnNewChatMessage.Field()
