import django_filters as filters
from django.db.models import Q

from django_filters import OrderingFilter
from graphql_relay import from_global_id

from apps.chat.models import ChatGroup, ChatMessage


class ChatMessageFilter(filters.FilterSet):
    group = filters.CharFilter(method='filter_by_group')
    date_from = filters.DateFilter(field_name='created_at', lookup_expr=('gt'))
    date_to = filters.DateFilter(field_name='created_at', lookup_expr=('lt'))
    search = filters.CharFilter(method='search_by')

    class Meta:
        model = ChatMessage
        fields = (
            'group',
        )

    order_by_field = 'ordering'
    ordering = OrderingFilter(
        fields=(
            'id',
            'created_at'
        )
    )

    def filter_by_group(self, queryset, name, value):
        group_id = from_global_id(value)[1]
        return queryset.filter(group__id=group_id)

    def search_by(self, queryset, name, value):
        return queryset.filter(
            Q(content__icontains=value)
        )


class ChatGroupFilter(filters.FilterSet):
    search = filters.CharFilter(method='search_by')

    class Meta:
        model = ChatGroup
        fields = (
            'title',
            'group_type'
        )

    order_by_field = 'ordering'
    ordering = OrderingFilter(
        fields=(
            'id',
            'created_at',
            'updated_at',
            ('last_message__created_at', 'last_message')
        )
    )

    def search_by(self, queryset, name, value):
        return queryset.filter(
            Q(title__icontains=value)
        )
