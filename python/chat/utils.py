import uuid


def upload_to_media(instance, filename):
    ext = filename.split('.')[-1]
    return f"./{uuid.uuid4().hex[:10]}.{ext}"


def upload_to_avatars(instance, filename):
    ext = filename.split('.')[-1]
    table_name = instance._meta.db_table
    return f"./avatars/{table_name}/{uuid.uuid4().hex[:10]}.{ext}"
