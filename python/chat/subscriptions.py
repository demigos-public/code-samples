import uuid

import channels_graphql_ws
import graphene

from api.v2.chat.types import ChatMessageType
from apps.chat.models import ChatMembership

TEST_MESSAGE_CONTENT = str(uuid.uuid4())


class OnNewChatMessage(channels_graphql_ws.Subscription):
    """Subscription triggers on a new chat message."""
    message = graphene.Field(ChatMessageType)

    def subscribe(self, info):
        """Client subscription handler."""

        del info
        return

    def publish(self, info):
        """Called to prepare the subscription notification message."""

        message = self["message"]

        if not ChatMembership.objects.filter(account_id=info.context.user.id,
                                             group_id=message.group.id):
            return OnNewChatMessage.SKIP

        return OnNewChatMessage(message=message)

    @classmethod
    def new_chat_message(cls, message):
        # This check is needed for test purposes
        if message.content == TEST_MESSAGE_CONTENT:
            return
        cls.broadcast(
            payload={
                "message": message
            }
        )
