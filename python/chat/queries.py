import graphene
from graphene import relay

from graphene_django.filter import DjangoFilterConnectionField
from graphql_jwt.decorators import login_required

from apps.chat.models import (
    ChatGroup, ChatMessage, ChatMembership,
    ChatMessageStatus
)
from .types import ChatGroupType, ChatMessageType
from .filters import ChatGroupFilter, ChatMessageFilter


class ChatQuery(graphene.ObjectType):
    chat_group_list = DjangoFilterConnectionField(
        ChatGroupType,
        filterset_class=ChatGroupFilter
    )
    chat_message_list = DjangoFilterConnectionField(
        ChatMessageType,
        filterset_class=ChatMessageFilter
    )
    chat_message_details = relay.Node.Field(ChatMessageType)
    chat_group_details = relay.Node.Field(ChatGroupType)
    chat_unread_messages_count = graphene.Int()

    @login_required
    def resolve_chat_group_list(self, info, **kwargs):
        user = info.context.user
        search_arg = kwargs.get('search')
        memberships = ChatMembership.objects.filter(account=info.context.user)
        group_list = ChatGroup.objects.filter(
            chatmembership__in=memberships,
            # last_message__isnull=False
        )
        if search_arg and search_arg in user.name:
            group_list = group_list.exclude(title__contains=user.name)
        return group_list

    @login_required
    def resolve_chat_message_list(self, info, **kwargs):
        return ChatMessage.objects.all()

    @login_required
    def resolve_chat_unread_messages_count(self, info, **kwargs):
        return ChatMessageStatus.objects.filter(
            account=info.context.user,
            is_read=False
        ).count()
